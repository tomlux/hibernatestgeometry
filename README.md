# README #

### hibernateSTgeometry ###

* provide 'STgeometry' support to hibernate (ESRI geodatabase)
* version 1.0

It's still beta, but first tests are very promising.
I'm using the WKT/WKB representation to read or update the geometry in the database.

It looks like this:

    @Entity
    @Table(name = "dau_track")
    public class DauTrack {

        @Column(columnDefinition = "st_geometry")
        @Type(type = "lu.schuller.arcgisStGeometry.WkbStGeometryType")
        @ColumnTransformer(read = "st_asbinary(shape)", write = "st_geomfromwkb(?,2169)")
        private Polyline shape;


I created my own Wkb-HibernateType to convert the wkb-data betweent database and geometry-api.
I have already implemented JTS and esri-geomeotry as geo-libraries.

The 'columnTransformers' are modifiing the generated hibernate-sql to read and write under wkb-form in the database.

It's currently running under postgresql and underOracle.
Feel free to contact me for any questions: tom@schuller.lu