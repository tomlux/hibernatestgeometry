package lu.schuller.hibernateSTgeometry.dialects;

import org.hibernate.dialect.PostgreSQL82Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.DoubleType;

/**
 * Created by schullto on 26/02/2015.
 */
public class STGeomPostgreSQL82Dialect extends PostgreSQL82Dialect {
    public STGeomPostgreSQL82Dialect() {
        super();
        registerFunction("st_length", new SQLFunctionTemplate(new DoubleType(), "sde.st_length(sde.st_geomfromwkb(?1))"));
    }
}
