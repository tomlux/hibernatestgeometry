package lu.schuller.hibernateSTgeometryTesting;

/**
 * User: schullto
 * Date: 23/12/2014
 * Time: 5:53 AM
 */

import com.esri.core.geometry.Polyline;
import lu.schuller.hibernateSTgeometryTesting.entity.MyLine;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

public class MyLineTester {

    /*
    for testing purposes, drag-drop the ./src/test/data/myline.shp into your geodabase
        and correct the annotations in the MyLine-class
     */

    public static void main(String[] args) {
        EntityManager em = Persistence.createEntityManagerFactory("myJPA").createEntityManager();

        List<MyLine> myLines = em.createQuery("from MyLine").getResultList();
        System.out.println("myLines = " + myLines.size());
        for (MyLine myLine : myLines) {
//            System.out.println("myLine = " + myLine);
        }

        em.getTransaction().begin();
        MyLine newLine = new MyLine();
        Polyline polyline = new Polyline();
        polyline.startPath(70000,70000);
        polyline.lineTo(72000,72000);
        newLine.setShape(polyline);
        newLine.setDsg("ABC");
        em.merge(newLine);
        em.getTransaction().commit();

        myLines = em.createQuery("from MyLine").getResultList();
        System.out.println("myLines1 = " + myLines.size());
        for (MyLine myLine : myLines) {
//            System.out.println("myLine = " + myLine);
        }
        myLines = em.createQuery("from MyLine where st_length(shape) > 2000").getResultList();
        System.out.println("myLines2 = " + myLines.size());

        em.close();
    }
}